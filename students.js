var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];
var maxIndex = -1, max;
/*выводим спиок студентов и ищем студента с максимальным баллом*/
for (i=0, imax = studentsAndPoints.length-1;i < imax; i+=2){
	console.log('Cтудент %s  набрал %d баллов',studentsAndPoints[i],studentsAndPoints[i+1]);
	if (maxIndex < 0 || studentsAndPoints[i+1] > max) {
		maxIndex = i;
		max = studentsAndPoints[i+1];
	};
};
/*выводим лучшего студента*/
console.log('');
console.log('Студент набравший максимальный балл:');
console.log('Студент %s имеет максимальный балл %d',studentsAndPoints[maxIndex],max);
/*добавляем новых студентов*/
studentsAndPoints.push('Николай Фролов',0,'Олег Боровой',0);
/*добавляем баллы*/
console.log('');
console.log('Изменения');
var findIndex = studentsAndPoints.indexOf('Антон Павлович');
if (findIndex !== -1) {
	studentsAndPoints[findIndex+1] +=10;
	console.log('Cтудент %s  набрал %d баллов',studentsAndPoints[findIndex],studentsAndPoints[findIndex+1]);
};
findIndex = studentsAndPoints.indexOf('Николай Фролов');
if (findIndex !== -1) {
	studentsAndPoints[findIndex+1] +=10;
	console.log('Cтудент %s  набрал %d баллов',studentsAndPoints[findIndex],studentsAndPoints[findIndex+1]);
};
console.log('');
/*выводим студентов с нулевыми баллами*/
console.log('Студенты не набравшие баллов:');
for (i=0, imax = studentsAndPoints.length-1;i < imax; i+=2){
	if (studentsAndPoints[i+1]==0){
		console.log(studentsAndPoints[i]);
	};
};

console.log("Удаляем записи");
/*обнуляем баллы всем*/
for (i=0, imax = studentsAndPoints.length-1;i < imax; i+=2){
	(studentsAndPoints[i+1]=0);
};

/*удаляем студентов с нулевыми баллами*/
for (i = studentsAndPoints.length-1;i > 0; i-=2){
	if (studentsAndPoints[i]==0){
		studentsAndPoints.splice(i-1, 2);
	};
};

console.log("записей в массиве %d",studentsAndPoints.length);
